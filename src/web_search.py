import json
import sys
from urllib import *
from urllib.parse import urlparse, urlencode, parse_qs
from urllib.request import  urlopen


YOUTUBE_COMMENT_URL = 'https://www.googleapis.com/youtube/v3/commentThreads'
YOUTUBE_SEARCH_URL = 'https://www.googleapis.com/youtube/v3/search'
YOUTUBE_VIDEO_URL = 'https://www.googleapis.com/youtube/v3/videos'
YOUTUBE_RECOMMENDED_URL = 'https://www.googleapis.com/youtube/v3/activities'
KEY = "AIzaSyDwGFeMy_ewBbqu1yJQuPt9tLeswI_oadM"

def search_youtube(search_text):
	matches = None
	mxRes = 30
	
	if search_text == None:
		parms = {
			'channelId': 'UChRCo7Pcx7xbAhfuyVeSjtQ',
			'part': 'id,snippet,contentDetails',
			'maxResults': mxRes,
			'regionCode': "IN",
			'key': KEY		
		}
	else:
		parms = {
			'q': search_text,
			'part': 'id,snippet',
			'maxResults': mxRes,
			'regionCode': "IN",
			'key': KEY		
		}
	#try:
	if search_text == None:
		matches = openURL(YOUTUBE_RECOMMENDED_URL, parms)
	else:
		matches = openURL(YOUTUBE_SEARCH_URL, parms)
	search_response = json.loads(matches)
	
	#i = 2
	#nextPageToken = search_response.get("nextPageToken")
	if search_text == None:
		matches = load_activity_res(search_response)
	else:
		matches = load_search_res(search_response)
	
	""""while nextPageToken:
	parms.update({'pageToken': nextPageToken})
	matches = self.openURL(YOUTUBE_SEARCH_URL, parms)

	search_response = json.loads(matches)
	nextPageToken = search_response.get("nextPageToken")
	print("Page : {} --- Region : {}".format(i, args.r))
	print("------------------------------------------------------------------")

	self.load_search_res(search_response)

	i += 1
	
	except KeyboardInterrupt:
		print("User Aborted the Operation")

	except:
		print("Cannot Open URL or Fetch comments at a moment")
	"""
	return matches


def openURL( url, parms):
	urlO = url + '?' + urlencode(parms)
	print(urlO)
	f = urlopen(urlO)
	data = f.read()
	f.close()
	matches = data.decode("utf-8")
	return matches


def load_activity_res(search_response):
	videos, channels, playlists = [], [], []
	for search_result in search_response.get("items", []):
		if search_result["kind"] == "youtube#activity":
			try:
				dataVideo = dict()
				dataVideo["title"] = "[Youtube]" + search_result["snippet"]["title"]
				dataVideo["id"] = search_result["contentDetails"]["like"]["resourceId"]["videoId"]
				dataVideo["thumbnail"] = search_result["snippet"]["thumbnails"]["default"]
				dataVideo["description"] = search_result["snippet"]["description"].replace("&", "")
				dataVideo["channelTitle"] = search_result["snippet"]["channelTitle"]
				videos.append(dataVideo)
			except:
				None
	data = dict()
	data["videos"] = videos
	data["channels"] = channels
	data["playlists"] = playlists
	return data
	
def load_search_res(search_response):
	videos, channels, playlists = [], [], []
	for search_result in search_response.get("items", []):
		
		if search_result["id"]["kind"] == "youtube#video":		
			dataVideo = dict()
			#videos.append("{} ({}) ({})".format(search_result["snippet"]["title"],
			#			     search_result["id"]["videoId"],
			#			     search_result["snippet"]["thumbnails"]["default"]["url"]))
			dataVideo["title"] = "[Youtube]" + search_result["snippet"]["title"]
			dataVideo["id"] = search_result["id"]["videoId"]
			dataVideo["thumbnail"] = search_result["snippet"]["thumbnails"]["default"]
			dataVideo["description"] = search_result["snippet"]["description"].replace("&", "")
			dataVideo["channelTitle"] = search_result["snippet"]["channelTitle"]
			videos.append(dataVideo)
	
		elif search_result["id"]["kind"] == "youtube#channel":
			channels.append("{} ({})".format(search_result["snippet"]["title"],
						       search_result["id"]["channelId"]))
		elif search_result["id"]["kind"] == "youtube#playlist":
			playlists.append("{} ({})".format(search_result["snippet"]["title"],
					search_result["id"]["playlistId"]))
	data = dict()
	data["videos"] = videos
	data["channels"] = channels
	data["playlists"] = playlists
	return data

def get_video_info(videoID):
	matches = None

	parms = {
		'part':'statistics,contentDetails',
		'id': videoID,
		'key': KEY		
	}

	try:
		matches = openURL(YOUTUBE_VIDEO_URL, parms)
		search_response = json.loads(matches)
		matches = load_statics_res(search_response)
		
	except KeyboardInterrupt:
		print("User Aborted the Operation")

	except:
		print("Cannot Open URL or Fetch comments at a moment")
	return matches


def load_statics_res(search_response):
	dataVideo = dict()
	for search_result in search_response.get("items", []):
		dataVideo["views"] = search_result["statistics"]["viewCount"]
		dataVideo["likeCount"] = search_result["statistics"]["likeCount"]
		dataVideo["dislikeCount"] = search_result["statistics"]["dislikeCount"]
		dataVideo["favoriteCount"] = search_result["statistics"]["favoriteCount"]
		dataVideo["duration"] = search_result["contentDetails"]["duration"]
		#dataVideo["licensedContent"] = search_result["contentDetails"]["licensedContent"]
	return dataVideo
	
