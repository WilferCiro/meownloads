'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Graphic library
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject, GLib, Gdk

# Translation
import locale
import gettext
_ = gettext.gettext

# Functions
from functions import App, return_ui_file
from web_search import search_youtube

from preview_download import previewDownload


import threading


class MainWindow(Gtk.Bin):

	def close_app(self,*args):
		Gtk.main_quit(*args)

	def run(self):
		try:
			Gtk.Bin.__init__(self)
			self.builder = Gtk.Builder()
			path = return_ui_file("MainWindow.ui")
			
			# Add Glade File
			self.builder.add_from_file(path)
			
			# Start Window and propierties
			self.window_Main = self.builder.get_object("MainWindow")
			self.window_Main.connect("delete_event", self.close_app)
			self.window_Main.set_position(Gtk.WindowPosition.CENTER)
			self.window_Main.set_default_size(1030, 630)
			self.window_Main.maximize()
			
			# Search bar
			self.__search_bar = self.builder.get_object("SongNameSearch")
			self.__search_bar.connect("activate", self.web_search)
			self.__search_loader = self.builder.get_object("SearchLoader")
			self.__search_loader.stop()
			
			# List Box search
			self.__list_box_download = self.builder.get_object("ListBoxDownloadSide")
			
			self.__load_default = True
			self.web_search(None)
			
			self.__label_operations = self.builder.get_object("LabelOperations")
			
			# Shortcuts	
			# TODO add all shortcuts
			accel = Gtk.AccelGroup()
			accel.connect(Gdk.keyval_from_name('S'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('R'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('U'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('F'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('J'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('N'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('F2'), 0, 0, self.on_accel_pressed)
			self.window_Main.add_accel_group(accel)

			self.window_Main.show_all()
			Gtk.main()			

		except KeyboardInterrupt:
			print ("\nExit on user cancel.")
			sys.exit(1)
	
	def on_accel_pressed(self, accel_group, acceleratable, keyval, modifier):
		if Gdk.ModifierType.CONTROL_MASK == modifier:
			if Gdk.keyval_name(keyval) == "s":
				print("Press S")
	
	
	def web_search(self, *args):
		try:
			while self.__list_box_download.get_children()[0]:
				self.__list_box_download.remove(self.__list_box_download.get_children()[0])
		except:
			None
	
		thread = threading.Thread(
				target=self.load_search,
				args=()
		)
		thread.daemon = True
		thread.start()
	
		
	def load_search(self):
		#try:
		self.__search_loader.start()
		if self.__load_default:
			self.__label_operations.set_text("Obtaining the videos I liked.")
			self.__load_default = False
			a = search_youtube(None)
		else:
			self.__label_operations.set_text("Searching \"" + self.__search_bar.get_text() + "\"")
			a = search_youtube(self.__search_bar.get_text())				
		
		for element in a["videos"]:
			GLib.idle_add(self.add_video, element)
			#raise TooSlowException
		#except:
		#	None
		self.__search_loader.stop()
		
	def add_video(self, element):
		row = Gtk.ListBoxRow()
		download_box = previewDownload(element)
		row.add(download_box)
		row.show_all()
	
		self.__list_box_download.insert(row, -1)
	
	
	def set_popover_prop(self):
		label = self.builder.get_object("LabelData")
		label.set_markup("Loading information...")
	
	def set_popover_prop_info(self, infor):
		label = self.builder.get_object("LabelData")
		text = infor["title"] + \
			"\n⏰ Duration: " + infor["duration"] + \
			"\n😊 Views: " + infor["views"] + \
			"\n😁 Likes: " + infor["likeCount"] + \
			"\n😭 Dislikes: " + infor["dislikeCount"] + \
			"\n😍 Favorite count: " + infor["favoriteCount"] + \
			"\n\n" + infor["description"]
		label.set_markup(text)
	
	def setPopoverOptionsDownload(self, widget, data):
		modal = self.builder.get_object("DownloadOptions")
		modal.set_relative_to(widget)
		modal.show()
		

class TooSlowException(Exception):
    pass	
	
	
