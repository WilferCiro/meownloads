
from gi.repository import Gtk, Gio
from functions import App, return_ui_file
from web_search import get_video_info

from gi.repository.GdkPixbuf import Pixbuf
from urllib.request import urlopen


import threading

class previewDownload(Gtk.Bin):

	def __init__(self, dataVideo):
		
		"""
            Init source view
        """
		Gtk.Bin.__init__(self)
		
		self.propierties = dict()
		self.propierties["title"] = None
		
		# Configure file
		path = return_ui_file("DownloadBox.glade")
		self.builder = Gtk.Builder()
		self.builder.add_from_file(path)
		self.builder.connect_signals(self)
		
		self.__box = self.builder.get_object("DownloadBox")
		self.add(self.__box)
		
		self.__downloadOptionsButton = self.builder.get_object("DownloadOptions")
		self.__downloadOptionsButton.connect("clicked", self.__showOptionsDownload)
		
		self.__buttonPropierties = self.builder.get_object("SongPropierties")
		self.__buttonPropierties.connect("clicked", self.clickedElement)
		
		self.dataVideo = dataVideo
		
		self.__song_id = dataVideo["id"]
		self.__song_name = self.builder.get_object("SongName")
		self.__song_name.set_markup(dataVideo["title"].replace("&", " ") + "\n <small><a href='https://www.youtube.com/watch?v=" + dataVideo["id"] + "'>https://www.youtube.com/watch?v=" + dataVideo["id"] + "</a></small>\nBy: " + dataVideo["channelTitle"])
		
		self.__download_options = dict()
		self.__download_options["format"] = "mp4"
		self.__download_options["quality"] = "mp4"
		self.__download_options["save_thumbnail"] = False
		self.__download_options["save_lyrics"] = False
		
		self.show_all()		
		
		#hilo1 = threading.Thread(target=self.set_thumbnail)
		#hilo1.daemon = True
		#hilo1.start()
	
	def set_thumbnail(self):
		try:
			response = urlopen(self.dataVideo["thumbnail"]["url"])
			input_stream = Gio.MemoryInputStream.new_from_data(response.read(), None) 
			pixbuf = Pixbuf.new_from_stream_at_scale(input_stream, self.dataVideo["thumbnail"]["width"], self.dataVideo["thumbnail"]["height"], True, None)
			self.builder.get_object("ThumbnailSong").set_from_pixbuf(pixbuf)
		except Exception as e:
			#print('Meow Error: Error obteniendo Imágen')
			#print(e)
			None
		
	def load_prop(self):
		self.propierties = get_video_info(self.__song_id)
		self.propierties["title"] = self.dataVideo["title"]
		self.propierties["description"] = self.dataVideo["description"]
		App().MainWindow.set_popover_prop_info(self.propierties)
		#self.__buttonPropierties.set_sensitive(True)
		
	def clickedElement(self, button):
		App().MainWindow.set_popover_prop()
		#self.__buttonPropierties.set_sensitive(False)
		if self.propierties["title"] is None:
			hilo1 = threading.Thread(target=self.load_prop)
			hilo1.daemon = True
			hilo1.start()
		else:
			App().MainWindow.set_popover_prop_info(self.propierties)
	
	
	def __showOptionsDownload(self, button):
		App().MainWindow.setPopoverOptionsDownload(button, self.__download_options)
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	"""def descargando_vista(self,d):
		if d['status'] not in ('downloading', 'finished','error'):
			return
		try:
			indice = self.threads_arreglo.index(threading.current_thread().ident)
		except:
			indice = 0
		if d['status'] == 'error':
			print('Meow Error: Error en: '+str(indice))
			return
		if d['status'] == 'finished':
			# Descargado ...
			print('Meow: Terminado')
			return			
		#_bar = self.builder.get_object("descargando_bar{0}".format(str(indice)))
		#_propiedades_descarga = self.builder.get_object("propiedades_descarga{0}".format(str(indice)))		

		if d['status'] != 'finished':
			
			if float(d['total_bytes'])>0 and d['downloaded_bytes']!=None:
				percent = float(d['downloaded_bytes'])/float(d['total_bytes'])
			else:
				percent = 1
			#_bar.set_fraction(percent)
			self.progresos[indice] = percent
			
			try:
				if self.video_descargando[indice] == '' or (self.es_playlist[indice] == True and (self.video_descargando[indice] == 'Playlist' or self.video_descargando[indice] == '')):
					partes = d.get('filename').split('/')
					titulo = partes[len(partes)-1]
					self.video_descargando[indice] = titulo
				else:
					titulo = self.video_descargando[indice]
				titulo = self.video_descargando[indice]
			except Exception as e:
				titulo =_('Descargando...')
				print(e)
			progreso = self.organiza_bytes(int(d['downloaded_bytes']))+' / '+self.organiza_bytes(int(d['total_bytes']))
			try:
				velocidad = self.organiza_bytes(int(d.get('speed')))+'/s - '+self.tiempo_formato(int(d.get('eta')))+'s eta'
			except:
				velocidad = '--KiB/s'
			texto_descarga = titulo[0:30]+'...   '+progreso+'    '+velocidad
		self.propiedades[indice] = texto_descarga			
		#_propiedades_descarga.set_label(self.texto_descarga)
	
	
	
	
	
	class DownloadData(object):
		def debug(self, msg):
			global texto_thread,estado_logger,descargando_playlist,thread_playlist_descargada
			if msg.find('Finished downloading playlist')!=-1:
				thread_playlist_descargada = threading.current_thread().ident
			if msg.find('[download]')==-1:
				texto_thread = msg
			elif texto_thread.find(':3puesmiau:3')==-1 and msg.find('[download]')!=-1 and (estado_logger == 3 or descargando_playlist == True):
				texto_thread = ':3puesmiau:3'
			if msg.find('error.URLError')!=-1:
				global error_logger
				error_logger = threading.current_thread().ident
			print('Meow Debug: '+msg)

		def warning(self, msg):
			#global logger_mensaje
			#logger_mensaje = msg
			pass

		def error(self, msg):
			global error_logger
			error_logger = threading.current_thread().ident
			print('Meow Error: '+msg)
	"""
	
	
