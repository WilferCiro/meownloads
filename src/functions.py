'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Files
import os
import sys

# Translation
import locale
import gettext
_ = gettext.gettext

# Graphic library
from gi.repository import Pango, Gio, GLib

APP_NAME = "meownloads"
VERSION = 0.1
App = Gio.Application.get_default

# Global Font			
def get_global_font():
	global_font = Pango.FontDescription(App().Preferences.get_value(App().Preferences.text_editor))
	return global_font

def return_ui_file(file):
	path = ""
	try:
		# TODO: change to accept special characters
		path = os.path.join(os.getcwd(), "ui", file)
		if not os.path.exists(path):
			raise NameError(_("System error") + " - 001")
	except:
		try:
			path = os.path.join(sys.prefix, "share", "ardunome", "ui", file)
			if not os.path.exists(path):
				raise NameError(_("System error") + " - 001")
		except:
			try:
				path = os.path.join(sys.prefix, "share", "arduinome_app", "ui", file)
				if not os.path.exists(path):
					raise NameError(_("System error") + " - 001")
			except Exception:
				raise SystemExit(_("Cannot load ui file") + " - 001")
	return path

def return_css_file():
	"""commands_dir = GLib.get_user_config_dir() + "/" + APP_NAME + "/"
	file_name = "style.css"
	if not os.path.isfile(commands_dir + file_name):
		look_pref_folder(commands_dir)
		file = open(commands_dir + file_name, "w+")
		file.close()
		print(commands_dir + file_name)
	return commands_dir + file_name"""
	return return_ui_file("style.css")

